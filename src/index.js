import React from 'react'
import ReactDOM from 'react-dom'
import Mixer from './components/mixer.js'

ReactDOM.render(<Mixer />, document.getElementById('content'))
