'use strict'
import React from 'react'
import Loadable from 'react-loadable'

const Wheel = Loadable({
  loader: () => import('./wheel'),
  loading: () => <div>Loading...</div>,
})

const Slider = Loadable({
  loader: () => import('./slider'),
  loading: () => <div>Loading...</div>
})

export default class Mixer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      error: '',
      inputs: '',
      outputs: '',
      knots: [0, 0, 0, 0, 0, 0, 0, 0, 0],
      sliders: [0, 0, 0, 0, 0, 0, 0, 0, 0]
    }
    this.messageIn = this.messageIn.bind(this)
  }

  componentDidMount () {

    navigator.requestMIDIAccess().then(
      midi => {
        let deviceIdIn = ''
        let deviceIdOut = ''
        
        midi.inputs.forEach(device => {

          if (device.name === 'WORLDE easy control MIDI 1') {
            deviceIdIn = device.id

            midi.inputs
              .get(deviceIdIn)
              .onmidimessage = this.messageIn
          }
            
        })
        
        midi.outputs.forEach(device => {
          if (device.name === 'WORLDE easy control MIDI 1') deviceIdOut = device.id
        })

        this.setState({
          error: '',
          inputs: midi.inputs.get(deviceIdIn),
          outputs: midi.outputs.get(deviceIdOut)
        })

      },
      error =>
        this.setState({error: error, inputs: '', outputs: ''})
    )
    
  }

  messageIn (message, err) {
    if (message.data[0]===176) {
      if ((message.data[1]>=14) && (message.data[1]<=22)) {
        let knots = this.state.knots
        knots[message.data[1]-14] = (message.data[2]/127)*270
        this.setState(Object.assign(this.state,{knots: knots}))
      }
      if ((message.data[1]>=3) && (message.data[1]<=11)) {
        let sliders = this.state.sliders
        sliders[message.data[1]-3] = (message.data[2]/127)*79
        this.setState(Object.assign(this.state,{sliders: sliders}))
      }
    }
  }
  
  render () {
    return (
        <div style={{display:'flex',flexDirection:'column'}}>
        <div style={{display:'flex'}}>
        {
          this.state.knots.map(
            (knot, i) => <Wheel key={'w'+i} angle={knot} style={{minWidth:'100px'}} />
          )
        }
        </div>
        <div style={{display:'flex'}}>
        {
          this.state.sliders.map(
            (slider, i) => <Slider key={'s'+i} channel={i} position={slider} style={{minWidth:'100px'}}/>
          )
        }
      </div>
      </div>
    )
  }
}
  
