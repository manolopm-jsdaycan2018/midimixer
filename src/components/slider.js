'use strict'
import React from 'react'

export default class Slider extends React.PureComponent {
  render () {
    return (
        <svg viewBox="0 0 36 100" style={this.props.style}>
        <g>
        <rect x="16" y="10" width="5" height="90" style={{fill:'#202020'}} />
        <rect x="18" y="11" width="1" height="88" style={{fill:'black'}} />
        </g>

        <g transform={`translate(0 -${this.props.position||0})`}>
        <rect x="8" y="91" width="21" height="1" style={{fill:'#A0A0A0'}} />
        <rect x="8" y="92" width="21" height="2" style={{fill:'#404040'}} />
        <rect x="8" y="94" width="21" height="1" style={{fill:'#000000'}} />
        <rect x="8" y="95" width="21" height="1" style={{fill:'#A0A0A0'}} />
        <rect x="8" y="96" width="21" height="2" style={{fill:'#404040'}} />
        </g>

        <text x='34' y='100' fontWeight='bolder' fontSize="25">{this.props.channel}</text>
        </svg>
    )
  }
}
