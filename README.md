# Midi Mixer

Este es un proyecto de juguete que preparé para la JSDayCAN2018

Usa webmidi y react para interactuar con un WordDE Easycontrol

La idea es que los movimientos que se hacen en el easycontrol se ven reflejados en la web

Para probarlo está en linea aquí: https://www.graph-ic.org/react-experiments/midi-demos/midimixer/public/

Para usarlo en local:
- npm run build